from orator.seeds import Seeder


class MeetsTableSeeder(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        with self.db.transaction():
            latina = self.db.table('users').where('username', 'latina').first()
            roberto = self.db.table('users').where('username', 'roberto').first()
            julio = self.db.table('users').where('username', 'julio').first()
            jorge = self.db.table('users').where('username', 'jorge').first()
            gringo = self.db.table('users').where('username', 'gringo').first()

            self.db.table('meets').insert([
                {'user_id': roberto.id, 'sala_id': 1},
                {'user_id': latina.id, 'sala_id': 1},
                {'user_id': julio.id, 'sala_id': 1},
                {'user_id': julio.id, 'sala_id': 2},
                {'user_id': jorge.id, 'sala_id': 2},
                {'user_id': gringo.id, 'sala_id': 2},
            ])

