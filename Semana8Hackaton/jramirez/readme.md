- python -m venv venv
- . venv/Scripts/activate
- pip install -r requirements.txt 
- touch app.py
- touch db.py
- touch utils.py
- touch readme.md

- python db.py make:migration create_users_table --table users --create
- python db.py migrate
- python db.py make:migration create_salas_table --table salas --create
- python db.py make:migration create_meets_table --table meets --create
- python db.py make:migration create_messages_table --table meets --create
- python db.py make:seed users_table_seeder
- python db.py make:seed salas_table_seeder
- python db.py make:seed meets_table_seeder
- python db.py make:seed messages_table_seeder
- python db.py migrate:refresh --seed

