import os
from flask import Flask, request
from flask_orator import Orator, jsonify
from orator.orm import belongs_to, has_many, belongs_to_many, Collection

import json
# Configuration
DEBUG = True
ORATOR_DATABASES = {
    'default': 'postgres',
    'postgres': {
        'driver': 'postgres',
        'host': 'localhost',
        'database': 'chat',
        'user': 'postgres',
        'password': '123456',
        'prefix': ''
    }
}
# ORATOR_DATABASES = {
#     'default': 'mysql',
#     'mysql': {
#         'driver': 'mysql',
#         'host': 'localhost',
#         'database': 'chat',
#         'user': 'root',
#         'password': 'pachaqtec',
#         'prefix': ''
#     }
# }

# Creating Flask application
app = Flask(__name__)
app.config.from_object(__name__)

# Initializing Orator
db = Orator(app)

class User(db.Model):
    __fillable__ = ['username', 'email', 'password']
    __hidden__ = ['pivot']

    @has_many('user_id')            #@has_many('foreign_key')
    def meets(self):
        return Meet
    @belongs_to_many('meets','sala_id','user_id') 
    def users(self):
        return Sala
    

class Meet(db.Model):
    @belongs_to
    def users(self):
        return User
    @belongs_to
    def salas(self):
        return 
    @has_many('meets_id')                #@has_many('foreign_key')
    def messages(self):
        return Message
    
    
class Sala(db.Model):
    __fillable__ = ['nombre']
    @has_many('sala_id')
    def meets(self):
        return Meet

    @belongs_to_many('meets','user_id','sala_id') 
    def users(self):
        return User

class Message(db.Model):
    __fillable__ = ['meets_id','content']
    @belongs_to('meets_id','id')               #@belongs_to('local_key', 'parent_key')
    def meets(self):
        return Meet
class ListarMensaje():
    def __init__(self,sala,usuario, mensaje):
        self.meets_id = mensaje.meets_id
        self.sala_id = sala.id
        self.sala_nombre = sala.nombre
        self.user_id = usuario.id
        self.username = usuario.username
        self.content = mensaje.content
        self.updated_at = mensaje.updated_at.to_datetime_string()
    def __str__(self):
        return self.content
    def toDicc(self):
        return {
        "meets_id": self.meets_id,
        "sala_id": self.sala_id,
        "sala_nombre": self.sala_nombre,
        "user_id": self.user_id,
        "username": self.username,
        "content": self.content,
        "updated_at": self.updated_at
        }

#MENSAJES:
@app.route('/messages', methods=['GET'])
def get_all_message():    
    mensaje = Message.all()
    
    collection = Collection([])
    for itemMensajeObj in mensaje:
        meetItem = itemMensajeObj.meets
        persona = User.find_or_fail(meetItem.user_id)
        salachat = Sala.find_or_fail(meetItem.sala_id)

        listarMensaje = ListarMensaje(salachat, persona, itemMensajeObj)
        collection = collection.push(listarMensaje.toDicc())    
    return jsonify(collection)  

@app.route('/messages', methods=['POST'])
def create_message():
   message = Message.create(**request.get_json())
   return jsonify(message)

@app.route('/messages/<int:sala_id>', methods=['GET'])
def get_all_message_by_SalaId(sala_id):
    mensaje = Message.all()
    
    collection = Collection([])
    for itemMensajeObj in mensaje:
        meetItem = itemMensajeObj.meets
        persona = User.find_or_fail(meetItem.user_id)
        salachat = Sala.find_or_fail(meetItem.sala_id)

        listarMensaje = ListarMensaje(salachat, persona, itemMensajeObj)
        collection = collection.push(listarMensaje.toDicc())
    collection = collection.where('sala_id', sala_id)
    return jsonify(collection)

#USUARIOS   
@app.route('/', methods=['GET'])
def home():
    return "Hola desde Flask ORATOR"

@app.route('/users',methods=['GET'])
def get_all_users():
    users = User.all()
    return jsonify(users)

@app.route('/users/<int:user_id>', methods=['GET'])
def get_user(user_id):
    user = User.find_or_fail(user_id)
    return jsonify(user)

@app.route('/users', methods=['POST'])
def create_user():
   user = User.create(**request.get_json())
   return jsonify(user)

@app.route('/users/<int:user_id>', methods=['PATCH'])
def update_user(user_id):
    user = User.find_or_fail(user_id)
    e_mail = request.json['email']
    print(e_mail)
    user.update(**request.get_json())
    return jsonify(user)

#SALAS
  
@app.route('/salas')
def get_all_salas():
   salas = Sala.all()
   return jsonify(salas)

@app.route('/salas/<int:sala_id>', methods=['GET'])
def get_sala(sala_id):
   sala = Sala.find_or_fail(sala_id)
   return jsonify(sala)

@app.route('/salas', methods=['POST'])
def create_sala():
   sala = Sala.create(**request.get_json())
   return jsonify(sala)

@app.route('/salas/<int:sala_id>', methods=['PATCH'])
def update_sala(sala_id):
   sala = Sala.find_or_fail(sala_id)
   sala.update(**request.get_json())
   return jsonify(sala)

def method_name():
   pass
if __name__ == '__main__':
    app.run(debug=True, port=8050)
    
