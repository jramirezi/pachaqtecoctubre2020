
  

# Hackatón Semana 6
## **LOGRO**: SQL, Modelos Entidad-Relación y Python
**“Empecemos a programar!!!”**
Llegó el momento de demostrar lo aprendido durante esta semana, para esto deberás cumplir un reto:


**Resuelve el problema en SQL, Python y súbelo a Gitlab**
### Insumos para resolver el Reto
 - Tener instalado Visual Studio Code 
 - Tener conocimientos básicos de SQL 
 - Tener conocimientos sobre Base de Datos
 
### Pasos a seguir para resolver el Reto
 - Descargar la ultima versión del proyecto
 - Crear la carpeta personal en la carpeta Semana6Hackaton
 - Resolver los problemas propuestos y guardarlos en varios archivos diferentes en la carpeta personal
 - Debe estar optimizado para **PostgreSQL**
 - Sube tus cambios a tu repositorio git.
 - Envía el **merge request** para integrar los cambios
### Solución del Reto
El proyecto debe tener una carpeta personal donde vamos a colocar programación requerida.
El problema a resolver es el siguiente:
- Crearemos un programa donde tengamos en cuenta lo siguiente: 
	 - Cliente: Nombre, Apellido 
	 - Mesero: Nombre, Apellido 
	 - Mesas: Numero
	 - Menú: Nombre, Valor 
 - Se deben tener en cuenta las siguiente pautas: 
	 - La orden es una sola transacción con la cabecera y el detalle

 - El repositorio deberá contar con commit's y su push en la rama principal (master)
### Reto cumplido
Para que el reto esté cumplido al 100% deberás tener cubierto los requisitos básicos expuestos:
 - Que se tenga en el gitlab 1 carpeta con el nombre (inicial del nombre y el apellido del estudiante) Dentro de ella, se agregarán los archivos de python y de texto plano los datos de las entidades
 - Revisar que el proyecto (repositorio) tenga un README.md con el nombre del colaborador y del proyecto.
 - Revisar que contengan commits y el push en la rama principal.

