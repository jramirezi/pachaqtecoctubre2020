--update cliente set nombre= 'Juan', apellido = 'Morales' where idcliente=1;
--select * from cliente where nombre like '%Jose%' or nombre like '%jose%';


insert into cliente (nombre, apellido)
values ('Jose Luis',' Soto Campos'),
		('Jose Armando','Espinoza Ramos'),
		('Juan Carlos','Linares Acurio'),
		('Jose Miguel','Gutierrez Soto'),
		('Julian','Perez Rivas'),
		('Julia Mila','Izarra Lira'),
		('Mario','Conde Tello'),
		('Carlos','Chu Chang'),
		('Percy','Chicoma Alvarado'),
		('Ruben','Mendoza Huaman'),
		('Juan Carlos','Agreda Mendoza'),
		('Julio','Primo Sotelo'),
		('María','Chancasana Flores'),
		('Miguel Angel','Terrones Albujar'),
		('Jose Maria','Pallet Huaman'),
		('Juan Eduardo','Alania Huamani'),
		('Sol Maria','Cabrejo Torres'),
		('Vicente Angel','Torres Ramirez'),
		('Angel Tirado','Ramirez Gutierrez'),
		('Jorge','Ponce Torres'),
		('Alexander Joel','Cordova Palacios'),
		('Alberto Omar','Prieto Martinez'),
		('Martin','Luna Espinoza'),
		('Christopher ','Solorzano Ramirez'),
		('Alex','Moreyra Rios'),
		('Pablo','Ramos Escobar'),
		('Ana María','Alvarez Torres'),
		('Antonio','Chia Chung'),
		('Gilberto','Ramirez Huamani'),
		('Rosa Maria','Mucha Yalo'),
		('Maria Milagros','Soto Cordova'),
		('Mario Daniel','Baldeon Pineda'),
		('Gino Alonso','Castro Lopez'),
		('David','Rios Menacho'),
		('Ana Raquel','Morales Molina'),
		('Dante','Dias Puerta'),
		('Fernando Miguel','Briones Palacios'),
		('Jose Enrique','Ramirez Molina'),
		('Ricardo','Ramirez Ruiz');

insert into mesero (nombre, apellido)
values ('Juan Miguel','Huamani Lopez'),
		('Julio ruben','Rodriguez Mendoza'),
		('Diego Pablo','Ramirez Davalos'),
		('Jose David','Padilla Huamani'),
		('Juan Mateo','Davila Machuca'),
		('Ernesto Miguel','Ramos Ramirez');

insert into menu (nombre, valor)
values ('Locro con queso',8),
		('Estofado de Pollo',10),
		('Seco de Pollo',10),
		('Escabeche de Pollo',12),
		('Pollo al Vino con Pure',12),
		('Chaufa',8),
		('Arroz con Pollo',12),
		('Arroz con Pollo con papa a la huancaina',12),
		('Papa Rellena',8),
		('Tallarin Saltado',10),
		('Sopa Menestron',8),
		('Sopa a la Minuta',8),
		('Aguadito',8),
		('Saltado de Higado',10),
		('Lomo Saltado',18),
		('Osobuco de carne',10),
		('Guiso de Vainita',8),
		('Ensala Cesar',12),
		('Causa de Pollo',8),
		('Tequeños',8),
		('Tallarin verde con bisteck',12),
		('Tallarin verde con Huancaina',12),
		('Papa a la Huancaina',8),
		('Dieta de Pollo',8),
		('Caldo Gallina',12),
		('Papa a la Huancaina',8),
		('Ocopa',8),
		('Arroz a la Jardinera', 8),
		('Sopa Sustancia',8),
		('Ensalada de Palta',8),
		('Pescado frito con Frejol',10),
		('Pescado frito con Lenteja',10),
		('Escabeche de Pescado',12),
		('Ceviche',25),
		('Sudado de pescado',15),
		('Arroz con Marisco',18),
		('Chilcano',10),
		('Milanesa con papas fritas',12),
		('Mondonguito a la italiana',8),
		('Olluquito con carne',8),
		('Pollo a la cocacola',10),
		('Asado de Res  con frecol o Papa',12),
		('Pollo asado con pure',12),
		('Chuleta frita con frejol',12),
		('Tallarin con pollo y papa a la Huancaina',12);

insert into mesa (numero)
values (1),
		(2),
		(3),
		(4),
		(5),
		(6),
		(7);

select * from cliente;
select * from mesero;
select * from menu;
select * from mesa;

SELECT LOCALTIMESTAMP
select CURRENT-TIME
select CURRENT_DATE
SELECT now();

insert into ordencabecera (idcliente, idmesero, idmesa, fecha) values (5,3,5, CURRENT_DATE);

SELECT idordencabecera FROM ordencabecera ORDER BY id{idOrdenCabecera}{idOrdenCabecera} desc limit 1;

select ordencabecera.idordencabecera, cliente.nombre as nombrecliente, cliente.apellido as apellidocliente, ordencabecera.idmesero, ordencabecera.idmesa, ordencabecera.fecha
from cliente inner join ordencabecera
on cliente.idcliente = ordencabecera.idcliente;

insert into ordencabecera (fecha, idcliente, idmesero, idmesa) values (CURRENT_DATE,5,3,2);
insert into ordencabecera (fecha, idcliente, idmesero, idmesa) values (CURRENT_DATE,2,2,3);
INSERT INTO ordendetalle (idordencabecera, idmenu, cantidad)
values (1,1,1),
		(1,2,2),
		(1,3,1),
		(2,3,2),
		(2,4,1),
		(2,5,2),
		(2,6,1);
SELECT * FROM ordendetalle;

select ordencabecera.idordencabecera, cliente.nombre as nombrecliente, cliente.apellido as apellidocliente, ordencabecera.idmesero, ordencabecera.idmesa, ordencabecera.fecha
from cliente inner join ordencabecera
on cliente.idcliente = ordencabecera.idcliente;

select *
from ordencabecera inner join cliente
on cliente.idcliente = ordencabecera.idcliente;

--Super Join:
SELECT ordencabecera.idordencabecera, cliente.nombre as nombrecliente, cliente.apellido as apellidocliente, mesero.nombre as nombreMesero, mesa.idmesa as nummeromesa, menu.nombre as plato, menu.valor as precio, ordencabecera.fecha
from ((((ordencabecera INNER JOIN cliente
ON cliente.idcliente = ordencabecera.idcliente)
inner JOIN mesero
ON mesero.idmesero = ordencabecera.idmesero)
INNER JOIN mesa
on mesa.idmesa = ordencabecera.idmesa)
inner Join ordendetalle
on ordendetalle.idordencabecera = ordencabecera.idordencabecera)
INNER JOIN menu
on menu.idmenu = ordendetalle.idmenu
where ordendetalle.idordencabecera = 1;