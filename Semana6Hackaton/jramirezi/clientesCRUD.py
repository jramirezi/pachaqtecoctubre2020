from utils import  Conexion
import utils
from modelos import Cliente, Mesero, Menu, Mesa, Orden, OrdenDetalle

class EjecutarCmd:
    
    def __init__(self):
        pass
    def Insertar(self,tabla, valor):     
        
        conn  = Conexion()
        nombreCol = conn.ConsultarNombreColumnas(tabla)
        idnom = nombreCol.pop(0)  # Quitamos el Id, pues no se necesita
        campos = ""
        
        for x in nombreCol[:-1]:
            campos = campos + x + ", "           
        campos = campos +nombreCol[-1]
        query = f"INSERT INTO {tabla} ({campos}) VALUES {valor};"
        conn  = Conexion()
        if(conn.ejecutarInsert(query,tabla,idnom)):          
            print(f"se insertó : ({valor}) con {idnom} ={conn.rowId}")
            return conn.rowId
        else:
           
            return 0
class ClienteList(EjecutarCmd):
    query = ""
    lstCliente = []
    def __init__(self, lstCliente=[]):
        self.lstCliente = lstCliente
        
    
    def Agregar(self, tupla):        #tupla = ('nombre', 'apellido')
        # conn = Conexion()
        # __id =conn.retornarNexttId('cliente','idcliente')   #Retorna una Tupla:> (48,)
        # __id = __id[0]                                     #Extraemos la posicion [0] 
        __id = self.Insertar('cliente',tupla)
        if(__id != 0):                       
            self.lstCliente.append(Cliente(__id, tupla[0], tupla[1]) )            
        else:
            print("Hubo un problema en la insersion de datos")
        return __id #Retorna un valor diferente de cero cuando se insertan datos

    def CargarData(self):
        conn = Conexion()
        self.query = 'Select * from cliente'
        result = conn.consultarBDD(self.query)
        print(conn.isOkeyQuery)
        self.lstCliente = []
        if(conn.isOkeyQuery):
            for objResult in result:
                objClass = Cliente(objResult[0],objResult[1],objResult[2] )
                #print(objClass)
                self.lstCliente.append(objClass)
            print("Data cargada, se retorna la lista de clientes ")
            return self.lstCliente
        else:
            self.lstCliente = []
            return self.lstCliente

    def Buscar(self):
        nombre = input("Escribe el Nombre del Cliente a buscar: ")
        nombre = nombre.capitalize()
        query = f"select * from cliente where nombre like '%{nombre}%' or nombre like '%{nombre.lower()}%';"
        conn  = Conexion()
        connResult = conn.consultarBDD(query)
        encontrados = {}
        if (connResult == []):
           print("No se encontraron resultados.. ")
           return False
        else:
            for objResult in connResult:
                print(f"codigo: {objResult[0]}, Nombres: {objResult[1]}, Aéllidos: {objResult[2]}")
            return True

    def BuscarPorID(self, ID):
        query = f"select * from cliente where idcliente = {ID};"
        conn  = Conexion()
        connResult = conn.consultarBDD(query)
        if (connResult == []):
            print("No se encontraron resultados.. ")
            return 0
        else:
            for objResult in connResult:
                cl = Cliente(objResult[0], objResult[1], objResult[2])
                return cl
        
    def Actualizar(self):
        
        idCliente = int(input("escoje el id del cliente que deseas Actualizar: "))
        nombre = input("Escribe del nuevo Nombre del Cliente a actualizar: ")
        apellido = input("Escribe del nuevo  Apellido del Cliente a actualizar : ")
        query = f"update cliente set nombre = '{nombre}', apellido = '{apellido}' where idcliente = {idCliente}"
        conn  = Conexion()
        connResult = conn.ejecutarBDD(query)
        if(connResult):
            print("Cliente Actualizado Correctamente")
            for indic, itemObj in enumerate( self.lstCliente):
                
                if itemObj.idcliente == idCliente:
                    #print("Bingo :",idCliente)
                    self.lstCliente[indic] = Cliente(idCliente,nombre,apellido)
                    print("Lista actualizada con los valores: ",  self.lstCliente[indic])
            #lstCliente = []
            #CargaInicial()
    def Borrar(self):
        idCliente = int(input("escoje el id del cliente que deseas Borrar : "))
        query = f"delete from cliente where idcliente = {idCliente};"
        conn  = Conexion()
        connResult = conn.ejecutarBDD(query)
        if(connResult):
            for indic, itemObj in enumerate( self.lstCliente):
                if itemObj.idcliente == idCliente:
                    print("Se ha Borrado Correctamente :",  self.lstCliente[indic])
                    self.lstCliente.pop(indic)
            #lstCliente = []
            #CargaInicial()
    def Imprimir(self):
        for itemObj in self.lstCliente:
            print(itemObj)
        return self.lstCliente
