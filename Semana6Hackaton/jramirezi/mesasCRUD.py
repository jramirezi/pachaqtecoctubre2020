from utils import  Conexion
import utils
from modelos import Cliente, Mesero, Menu, Mesa, Orden, OrdenDetalle

class EjecutarCmd:
    
    def __init__(self):
        pass
    def Insertar(self,tabla, valor):     
        
        conn  = Conexion()
        nombreCol = conn.ConsultarNombreColumnas(tabla)
        idnom = nombreCol.pop(0)  # Quitamos el Id, pues no se necesita
        campos = ""
        
        for x in nombreCol[:-1]:
            campos = campos + x + ", "           
        campos = campos +nombreCol[-1]
        query = f"INSERT INTO {tabla} ({campos}) VALUES ({valor});"
        conn  = Conexion()
        if(conn.ejecutarInsert(query,tabla,idnom)):          
            print(f"se insertó : ({valor}) con {idnom} ={conn.rowId}")
            return conn.rowId
        else:
           
            return 0
class MesaList(EjecutarCmd):
    query = ""
    lstMesa = []
    def __init__(self, lstMesa=[]):
        self.lstMesa = lstMesa
        
    
    def Agregar(self, data):        #tupla = ('nombre', 'apellido')
        
        __id = self.Insertar('mesa',data)
        if(__id != 0):
            self.lstMesa.append(Mesa(__id, data))           
        else:
            print("Hubo un problema en la insersion de datos")

    def CargarData(self):
        conn = Conexion()
        self.query = 'Select * from mesa'
        result = conn.consultarBDD(self.query)
        print(conn.isOkeyQuery)
        self.lstMesa = []
        if(conn.isOkeyQuery):
            for objResult in result:
                objClass = Mesa(objResult[0],objResult[1] )
                #print(objClass)
                self.lstMesa.append(objClass)
            print("Data cargada, se retorna la lista de mesas ")
            return self.lstMesa
        else:
            self.lstMesa = []
            return self.lstMesa

    def Buscar(self):
        numero = int(input("Escribe el Numero de la mesa a buscar: "))
        query = f"select * from mesa where numero = {numero};"
        conn  = Conexion()
        connResult = conn.consultarBDD(query)
        if (connResult == []):
           print("No se encontraron resultados.. ")
        else:
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}")
        
    def Actualizar(self):
        
        idmesa = int(input("escoje el id del mesa que deseas Actualizar: "))
        numero = int(input("Escribe del nuevo numero de Mesa que deseas actualizar: "))
        query = f"update mesa set numero = '{numero}' where idmesa = {idmesa}"
        conn  = Conexion()
        connResult = conn.ejecutarBDD(query)
        if(connResult):
            print("menu Actualizado Correctamente")
            for indic, itemObj in enumerate( self.lstMesa):
                
                if itemObj.idmesa == idmesa:
                    #print("Bingo :",idmenu)
                    self.lstMesa[indic] = Mesa(idmesa,numero)
                    print("Lista actualizada con los valores: ",  self.lstMesa[indic])
            #lstMesa = []
            #CargaInicial()
    def Borrar(self):
        idmesa = int(input("escoje el id de mesa que deseas Borrar:  "))
        query = f"delete from mesa where idmesa = {idmesa};"
        conn  = Conexion()
        connResult = conn.ejecutarBDD(query)
        if(connResult):
            for indic, itemObj in enumerate( self.lstMesa):
                if itemObj.idmesa == idmesa:
                    print("Se ha Borrado Correctamente :",  self.lstMesa[indic])
                    self.lstMesa.pop(indic)
            #lstMesa = []
            #CargaInicial()
    def Imprimir(self):
        for itemObj in self.lstMesa:
            print(itemObj)
        return self.lstMesa
