import psycopg2
from psycopg2 import Error as pgError

import os
from time import sleep
from os import system, name

class Conexion:
    isOkeyQuery = False
    
    def __init__(self, server='127.0.0.1', usr='postgres', psw='123456', bd='pchqtec10'):
        self.db = psycopg2.connect(
            host=server, user=usr, password=psw, database=bd)
        self.cursor = self.db.cursor()
        self.rowId = 0
        self.num_fields = 0
        self.field_names = []
        #self.isOkeyQuery = False
        #print("Se ha conectado correctamente a la base de datos") #Solo Para debug
    
    def retornarNexttId(self,tabla,idName):
        try:
            cur = self.cursor
            cur.execute(f"SELECT nextval('{tabla}_{idName}_seq');")
            records = cur.fetchone()            
            return records
            self.isOkeyQuery = False
        except Exception as error:
            print(error)
            self.isOkeyQuery = False
            return "falla"
        finally:
            cur.close()

    def ConsultarNombreColumnas(self, tabla):
        try:
            cur = self.cursor
            cur.execute(f"SELECT * FROM {tabla}")
            records = cur.fetchone()
            self.isOkeyQuery = True
            self.num_fields = len(cur.description)
            self.field_names = [i[0] for i in cur.description]
            return self.field_names
        except Exception as error:
            print(error)
            self.isOkeyQuery = False
            return []
        finally:
            cur.close()
    def consultarBDD(self, query):
        try:
            cur = self.cursor
            cur.execute(query)
            records = cur.fetchall()
            self.isOkeyQuery = True
            return records
        except Exception as error:
            print(error)
            self.isOkeyQuery = False
            return False
        finally:
            cur.close()

    def ejecutarBDD(self, query):
        try:
            cur = self.cursor
            cur.execute(query)
            self.db.commit()
            exito = True
            return exito
        except Exception as identifier:
            print(identifier)
            return False
        finally:
            cur.close()
    def ejecutarInsert(self, sql, tabla, idName): # val es una tupla (valor,otrovalor)
        try:
            cur = self.cursor
            cur.execute(sql)
            self.db.commit()
            cur.execute(f"SELECT * from {tabla} Limit 1;")
            cur.fetchone()
            cur.execute(f"SELECT currval('{tabla}_{idName}_seq');")
            self.rowId = cur.fetchone()
            self.rowId = self.rowId[0]
            exito = True
            return exito
        except Exception as identifier:
            print(identifier)
            return False
        finally:
            cur.close()
def Color(nomColor):
    color = {
    'white':    "\033[1;37m",
    'yellow':   "\033[1;33m",
    'green':    "\033[1;32m",
    'blue':     "\033[1;34m",
    'cyan':     "\033[1;36m",
    'red':      "\033[1;31m",
    'magenta':  "\033[1;35m",
    'black':      "\033[1;30m",
    'darkwhite':  "\033[0;37m",
    'darkyellow': "\033[0;33m",
    'darkgreen':  "\033[0;32m",
    'darkblue':   "\033[0;34m",
    'darkcyan':   "\033[0;36m",
    'darkred':    "\033[0;31m",
    'darkmagenta':"\033[0;35m",
    'darkblack':  "\033[0;30m",
    'off':        "\033[0;0m"
    }
    return color.get(nomColor)
class Menu:
    def __init__(self, NombreMenu, dicOpciones):
        self.NombreMenu = NombreMenu
        self.dicOpciones = dicOpciones
        
    def MostrarEncabezadoMenu(self, titulo):
        print(Color('magenta') +
                  "\t\t::::::::::::: Restaurante el Buen provecho::::::::::::::"+Color('off'))
        print(Color('blue')+"\t\t  :::::::::::::" + titulo + "::::::::::::::"+Color('off'))
        
    def MostrarMenu(self):
        self.LimpiarPantalla()
        opSalir = True        
        while(opSalir):
            
            self.LimpiarPantalla()
            self.MostrarEncabezadoMenu(self.NombreMenu)
            
            for (key, value) in self.dicOpciones.items():
                print(Color('green') + key + Color('off'), " :: ", value)
            #print("Salir :: 9")
            opcion = 100
            try:
                print(Color('darkmagenta') + "Escoge tu opcion:" + Color('off'))
                opcion = int(input())
            except ValueError as error:
                print("Opcion invalida deben ser numeros de 0 - 9")
            contOpciones = 0
            for (key, value) in self.dicOpciones.items():
                if(opcion == int(value)):
                    contOpciones += 1
            if(opcion == 8):
                opSalir = True
            if(opcion == 9):
                print("saliendo ... ")
                sleep(3)
                opSalir = False
                contOpciones = 1
                self.LimpiarPantalla() 
            if(contOpciones == 0):
                print("Escoge una opcion valida")
                sleep(5)
                          
            else:
                opSalir = False

        return opcion

    def LimpiarPantalla(self):
        def Clear():
            if name == 'nt':
            # return os.system('cls')
                return system('cls')
            else:
                # return os.system('cls')
                return system('clear')
        Clear()