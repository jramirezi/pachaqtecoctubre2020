
  

# Hackatón Semana 7
## **LOGRO**: Chat en Python con SQL y No-SQL 
**“Empecemos a programar!!!”**
Llegó el momento de demostrar lo aprendido durante esta semana, para esto deberás cumplir un reto:


![](chat.png)



**Resuelve el problema en SQL, Python y súbelo a Gitlab**
### Insumos para resolver el Reto
 - Tener instalado Visual Studio Code 
 - Tener conocimientos básicos de SQL 
 - Tener conocimientos sobre Base de Datos
 
### Pasos a seguir para resolver el Reto
 - Descargar la ultima versión del proyecto
 - Crear la carpeta personal en la carpeta Semana7Hackaton
 - Resolver los problemas propuestos y guardarlos en varios archivos diferentes en la carpeta personal
 - Debe estar optimizado para **PostgreSQL**
 - Sube tus cambios a tu repositorio git.
 - Envía el **merge request** para integrar los cambios
### Solución del Reto
El proyecto debe tener una carpeta personal donde vamos a colocar programación requerida.
El problema a resolver es el siguiente:
- Crearemos un programa donde tengamos en cuenta lo siguiente: 
	 - Usuario: username, email, password y fecha de creación
	 - Sala: Nombre, fecha de creación 
	 - Reunión: Numero
 - Se deben tener en cuenta las siguiente pautas: 
	 - Menú para registrar usuario
	 - Menú para registrar sala
	 - Menú para buscar e ingresar a la sala
	 - Menú para chatear
	 - El registro de usuario, sala y reunión se guardará en la base de datos relacional
	 - Los mensajes de chat se guardará en la base de datos no relacional
	 - La actualización de los mensajes se hará cada 5 segundos

 - El repositorio deberá contar con commit's y su push en la rama principal (master)
### Reto cumplido
Para que el reto esté cumplido al 100% deberás tener cubierto los requisitos básicos expuestos:
 - Que se tenga en el gitlab 1 carpeta con el nombre (inicial del nombre y el apellido del estudiante) Dentro de ella, se agregarán los archivos de python 
 - Revisar que el proyecto (repositorio) tenga un README.md con el nombre del colaborador y del proyecto.
 - Revisar que contengan commits y el push en la rama principal.
