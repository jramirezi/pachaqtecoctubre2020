CREATE TABLE Usuario (
  iduser SERIAL NOT NULL,
  username VARCHAR(16) NOT NULL,
  email VARCHAR(255) NULL,
  password VARCHAR(32) NOT NULL,
  create_time TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (iduser));


-- -----------------------------------------------------
-- Table sala
-- -----------------------------------------------------
CREATE TABLE sala (
  idsala SERIAL NOT NULL,
  nombresala VARCHAR(45) NOT NULL,
  create_time TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (idsala))



-- -----------------------------------------------------
-- Table Reunion
-- -----------------------------------------------------
CREATE TABLE Reunion (
  iduser INT NOT NULL,
  idsala INT NOT NULL,
  PRIMARY KEY (iduser, idsala),
  CONSTRAINT fk_user_has_sala_user
    FOREIGN KEY (iduser)
    REFERENCES Usuario (iduser)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_user_has_sala_sala1
    FOREIGN KEY (idsala)
    REFERENCES sala (idsala)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);