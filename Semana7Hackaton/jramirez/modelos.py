class Reunion:
    def __init__(self, iduser, idsala):
        self.iduser = iduser
        self.idsala = idsala
    
class Sala:
    def __init__(self, idsala, nombresala, create_time):
        self.idsala = idsala
        self.nombresala = nombresala
        self.create_time = create_time

class Usuario:
    def __init__(self, iduser, username, email, password, create_time):
        self.iduser = iduser
        self.username = username
        self.email = email
        self.password = password
        self.create_time = create_time

