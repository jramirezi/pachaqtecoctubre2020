## Quiero vender Helados

class Producto:
    __Almacen = "Bodega 1"
    def __init__(self, Codigo, Sabor, Nombre, Precio, Presentacion):
        self.Codigo = Codigo
        self.Sabor = Sabor
        self.Nombre = Nombre
        self.Precio = Precio
        self.Presentacion = Presentacion

    # def getAlmacen(self):
    #     return self.__Almacen
    
    # def setAlmacen(self, nuevoAlmacen):
    #     self.__Almacen = nuevoAlmacen

    @property
    def Almacen(self):
        return self.__Almacen
    
    @Almacen.setter
    def Almacen(self, nuevoAlmacen):
        self.__Almacen = nuevoAlmacen

    def Vender(self):
        print("Estoy vendiendo")

    def Comprar(self):
        print("Estoy comprando")

    def Almacenar(self):
        print("Estoy Almacenando en " + self.__Almacen)

########################################################################    
objProducto = Producto(1,"Chocolate","Helado",1.50,"Unidad")
objGaseosa = Producto(2,"Anís","IncaCola",5,"1LT")

print(objProducto.Nombre)
objProducto.Vender()
objProducto.Comprar()
objProducto.Almacenar()
print(objProducto.Almacen)
objProducto.Almacen = "Bodega 2"
print(objProducto.Almacen)
objProducto.Almacenar()
  
print(objGaseosa.Nombre)
objGaseosa.Vender()
objGaseosa.Comprar()
objGaseosa.Almacenar()
print(objGaseosa.Almacen)
objGaseosa.Almacen = "Bodega 3"
print(objGaseosa.Almacen)
objGaseosa.Almacenar()

class Persona:
    def __init__(self, DNI, Nombre, Apellido, FechaNacimiento, Sexo):
        self.DNI = DNI
        self.Nombre = Nombre
        self.Apellido = Apellido
        self.FechaNacimiento = FechaNacimiento
        self.Sexo = Sexo
    
objPersona = Persona("001575294","Roberto","Pineda","28/08/1983","Masculino")
print(objPersona.FechaNacimiento)

class Cliente(Persona):
    def __init__(self, DNI, Nombre, Apellido, FechaNacimiento, Sexo, CodigoCliente):
        super().__init__(DNI, Nombre, Apellido, FechaNacimiento, Sexo)
        self.CodigoCliente = CodigoCliente

objCliente = Cliente("001676345", "David", "Lopez", "13/11/1989", "Masculino", 100)

print(objCliente.Nombre)
print(objCliente.CodigoCliente)

lstProductos = []
lstProductos.append(objProducto)
lstProductos.append(objGaseosa)

print(lstProductos)

for obj in lstProductos:
    print(obj.Sabor)
    obj.Vender()

# Caracteristicas
#     DNI
#     Nombre
#     Apellido
#     FechaNacimiento
#     Sexo

# Cliente
#     CodigoCliente

#     Comprar
#     Devolver

# Vendedor
#     CodigoVendedor

#     Vender
#     RecibirDevolucion

# Proveedor
#     CodigoProveedor

#     Vender
#     RecibirDevolucion

