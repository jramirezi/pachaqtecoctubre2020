from orator.seeds import Seeder


class UsersTableSeeder(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        self.db.table('users').insert({
            'username': 'karen',
            'email': 'karen@x-codec.net',
            'password':'12345'
        })
        self.db.table('users').insert({
            'username': 'roberto',
            'email': 'rpineda@x-codec.net',
            'password' : '12345'
        })
        self.db.table('users').insert({
            'username': 'david',
            'email': 'dlopez@x-codec.net',
            'password' : '12345'
        })

