

import utils
import json
from modelos import Producto, Empleado, Cliente
from utils import Menu
from time import sleep

# for linea in range(1,20,3):
#     print(linea)

# opciones = {"Primera Opcion": 1, "Segunda Opcion": 2}
# menu = utils.Menu("Menu Principal", opciones)

# opcion = menu.MostrarMenu()
# print(opcion)
# if(opcion == 1):
#     otrasOpciones = {"Alianza":1, "Universitario":2}
#     nuevoMenu = utils.Menu("Equipos", otrasOpciones)
#     respuesta = nuevoMenu.MostrarMenu()
#     if(respuesta == 1):
#         print("Eres del Alianza")
#     elif(respuesta == 2):
#         print("Eres de Univesitario")
#     else:
#         print("No hat opcion")

# File = utils.FileManager("productos.md")
# File.escribirArchivo("Hola")
# print(File.leerArchivo())

# File2 = utils.FileManager("alumnos.md")
# File2.escribirArchivo("Alumno1")
# print(File2.leerArchivo())
# File2.borrarArchivo()

fileProducto = utils.FileManager("Productos.txt")
fileCliente = utils.FileManager("Clientes.txt")
fileEmpleado = utils.FileManager("Empleados.txt")
lstCliente = []
lstClienteDic = []
lstEmpleado = []
lstEmpleadoDic = []
lstProductos = []
lstProductosDic = []


def cargaInicial():
    try:
        try:
            res = fileProducto.leerArchivo()
            lstProducto = json.loads(res)
            for dicProducto in lstProducto:
                #codProducto, nombreProducto, cantidadProducto, costoProducto
                objProducto = Producto(dicProducto["codProducto"], dicProducto["nombreProducto"],
                                       dicProducto["cantidadProducto"], dicProducto["costoProducto"],
                                       dicProducto["TotalXProducto"], dicProducto["TotalProductos"])
                lstProductos.append(objProducto)
                lstProductosDic.append(dicProducto)

        except FileNotFoundError as error:
            fileProducto.escribirArchivo("")

        except Exception as error:
            print(error)

        try:

            res = ""
            res = fileCliente.leerArchivo()

            lstClientetmp = json.loads(res)
            for dicCliente in lstClientetmp:
                #dni, nombre, apellido, edad, codCliente
                objCliente = Cliente(dicCliente["codCliente"], dicCliente["nombre"],
                                     dicCliente["apellido"],
                                     dicCliente["edad"], dicCliente["dni"])
                lstCliente.append(objCliente)
                lstClienteDic.append(dicCliente)
        except FileNotFoundError as error:
            fileCliente.escribirArchivo("")
        except Exception as error:
            print(error)

        res = ""
        res = fileEmpleado.leerArchivo()
        lstEmpleado = json.loads(res)
        for dicEmpleado in lstEmpleado:
            #dni, nombre, apellido, edad, codEmpleado
            objEmpleado = Empleado(dicEmpleado["codEmpleado"], dicEmpleado["nombre"],
                                   dicEmpleado["apellido"], dicEmpleado["dni"],
                                   dicEmpleado["edad"])
            lstEmpleado.append(objEmpleado)
            lstEmpleadoDic.append(dicEmpleado)
    except FileNotFoundError as error:
        fileEmpleado.escribirArchivo("")
    except Exception as error:
        print(error)


def buscarobjeto(nombreobjeto, lstbuscar, strnombrebuscar):
    for objbuscar in lstbuscar:
        if objbuscar[nombreobjeto] == strnombrebuscar:
            return objbuscar
        else:
            return False


def validarobjeto(nombreobjeto, lstvalidar, strnombrevalidar):
    for objvalidar in lstvalidar:
        if objvalidar[nombreobjeto] == strnombrevalidar:
            return True
        else:
            return False


cargaInicial()

dicOpcionesMenuPrincipal = {"Cliente": 1, "Empleado": 2}
menuPrincipal = Menu("Menu de Inicio", dicOpcionesMenuPrincipal)
opcionMenuPrincipal = menuPrincipal.MostrarMenu()


dicOpcionesCrearProducto = {"Crear otro": 1,
                            "Mostrar todos": 2, "Eliminar Producto": 3}
menuProducto = Menu("Menu Producto", dicOpcionesCrearProducto)

dicOpcionesCrearCliente = {"Crear otro": 1, "Mostrar todos": 2}
menuClientesub = Menu("Menu Cliente", dicOpcionesCrearCliente)

dicOpcionesCrearEmpleado = {"Crear otro": 1, "Mostrar todos": 2}
menuEmpleadosub = Menu("Menu Cliente", dicOpcionesCrearCliente)

if(opcionMenuPrincipal == 9):
    opcionMenuPrincipal = menuPrincipal.MostrarMenu()

elif(opcionMenuPrincipal == 1):
    dicOpcionesCliente = {"Registrar Cliente": 1, "Buscar Cliente": 2}
    menuCliente = Menu("Menu de Cliente", dicOpcionesCliente)
    resmenucliente = menuCliente.MostrarMenu()
    salirCreacionCliente = True
    try:
        while salirCreacionCliente:
            if(resmenucliente == 1):

                print("Digita el Codigo del Cliente")
                codCliente = input()
                print("Digita el Nombre del Cliente")
                nomCliente = input()
                print("Digita el apellido del Cliente")
                apeCliente = input()
                print("Digita el D.N.I del Cliente")
                dniCliente = input()
                print("Digita la edad del Cliente")
                edadCliente = input()
                if validarobjeto("dni", lstClienteDic, dniCliente):
                    print("El Cliente ya existe")
                    sleep(10)
                    resretornarmenucliente = menuCliente.MostrarMenu()
                else:
                    cliente = Cliente(codCliente, nomCliente,
                                      apeCliente, dniCliente, edadCliente)
                print("Haz creado el cliente: ", cliente)
                fileCliente.borrarArchivo()
                lstClienteDic.append(cliente.dictCliente())
                lstCliente.append(cliente)
                jsonStr = json.dumps(lstClienteDic)
                fileCliente.escribirArchivo(jsonStr)
                resMenuClientesub = menuClientesub.MostrarMenu()
                if(resMenuClientesub == 1):
                    print("ingreso a la opcion 1 del menuCliente")
                elif(resMenuClientesub == 2):
                    print("ingreso a la opcion 2 del menuCliente")
                    for objCliente in lstCliente:
                        print(
                            f"|{objCliente.codCliente} | {objCliente.nombre} | {objCliente.apellido} | {objCliente.edad} | {objCliente.dni} |")
                    sleep(10)
                    resretornarmenucliente = menuCliente.MostrarMenu()
                    if(resretornarmenucliente == 1):
                        print(f"ingreso a la opcion {resretornarmenucliente}")
                else:
                    print(
                        f"ingreso a la opcion {resMenuClientesub} de menuCliente")
                    salirCreacionCliente = False
                    break
            elif(resmenucliente == 2):
                print("Digita el DNI del Cliente")
                strDniCliente = input()
                objetoencontrado = buscarobjeto(
                    "dni", lstClienteDic, strDniCliente)
                if objetoencontrado["codCliente"] != "":
                    print(
                        f"|{objetoencontrado ['codCliente']} | {objetoencontrado['nombre']} | {objetoencontrado['apellido']} | {objetoencontrado['edad']} | {objetoencontrado['dni']} |")
                    sleep(10)
                    res = menuCliente.MostrarMenu()
                else:
                    print("No se encuentra el cliente a buscar")
                # break
            else:
                pass
    except Exception as error:
        print(error)
