- python -m venv venv
- . venv/Scripts/activate
- pip install -r requirements.txt 
- touch app.py
- touch db.py
- touch utils.py
- touch readme.md

- python db.py make:migration create_movies_table --table movies --create
- python db.py migrate
- python db.py make:seed vods_movies_seeder
- python db.py migrate:refresh --seed

