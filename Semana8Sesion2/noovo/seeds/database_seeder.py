from orator.seeds import Seeder
from .movies_table_seeder import MoviesTableSeeder

class DatabaseSeeder(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        self.call(MoviesTableSeeder)

