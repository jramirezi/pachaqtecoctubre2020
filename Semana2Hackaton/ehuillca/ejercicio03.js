/** Determinar la primera palabra mas larga */
function LimpiarCadena(cadena){
    variosEspacios=/[ ]+/g; //Expresion regular que identifica a muchos espacios en la cadena
    cadena=cadena.trim(); // limpiamos los espacios en blanco al inicio y al final
    cadena=cadena.replace(variosEspacios," "); 
    return cadena;
}

function PalabraMasLarga(cadena){    
    cadena=LimpiarCadena(cadena);
    palabra=cadena.split(" "); //partimos la frase descpues de cada espacio
    var palabraLarga=palabra[0];
    
    for(i=1; i < palabra.length; i++){
        if (palabraLarga.length < palabra[i].length) palabraLarga=palabra[i];
    }
    
    console.log("La palabra mas larga es '" + palabraLarga + "' y tiene " + palabraLarga.length + " caracteres");

}

console.log(PalabraMasLarga("el peru es un paiss muy ricoss"));