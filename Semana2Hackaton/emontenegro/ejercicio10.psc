SubProceso res <- factorial ( num )
	Si(num = 1) Entonces
		res = 1
	SiNo
		res = num * factorial(num-1)
	FinSi
Fin SubProceso


Proceso ejercicio10
	Escribir "Factorial de un numero"
	Escribir "Ingrese numero: "
	Leer num
	Escribir "El factorial del ", num, " es: ", factorial(num)
FinProceso
