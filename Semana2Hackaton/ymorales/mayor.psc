Proceso numMayor
	//- Escribir Pseudoc�digo que lea de entrada 3 n�meros y que indique cual es el mayor de ellos.
	//DV
	Definir num1,num2,num3 Como Real
	Definir res Como Real
	
	//Entrada
	Escribir "Ingresa 3 n�meros"
	Leer num1, num2, num3
	
	//Proceso 
	Si num1 > num2 y num1 > num3 Entonces
		res = num1
	SiNo
		si num2 > num1 y num2 > num3 Entonces
			res = num2
		SiNo
			res = num3
		FinSi
	FinSi
	
	//salida
	Escribir "El mayor es: ",res
	
FinProceso
