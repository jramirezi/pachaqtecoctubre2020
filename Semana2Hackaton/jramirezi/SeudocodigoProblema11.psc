Proceso TablaMultiplicar
	// Hacer un Pseudocodigo que despliegue las tablas de multiplicar.
	
	Definir i, n Como Entero
	Escribir "===== Tabla de Multiplicar ======="
	Escribir " "
		i=1
		n=1
		Para n=1 Hasta 12 Con Paso 1 Hacer
			Escribir "======= Tabla de Multiplicar del ",n," ======"
			Escribir " "
			Para i=1 Hasta 12 Con Paso 1 Hacer
				Escribir n," X" i, "= ",n*i 
			Fin Para
			i=1
			Escribir "=============================================="
			Escribir " "
		Fin Para
	
FinProceso
