//Dados 10 n�meros enteros que se ingresan por teclado, calcular cu�ntos de ellos son pares, cu�nto suman ellos y el promedio de los impares
Proceso leer10Enteros
	
	Dimension num[10]
	definir i,x Como Entero
	definir nPares,nImpares, sumaTotal, sumaImpares Como Entero
	definir promImpares Como Real
	Escribir "=== Programa que pide ingresar 10 n�meros y calcula cu�ntos de ellos son pares, cu�nto suman ellos y el promedio de los impares=="
	Escribir " "
	Escribir "Es necesario Ingresar 10 n�meros :"
	Para i=1 Hasta 10 Con Paso 1 Hacer
		Escribir "Ingrese el numero ",i," :"
		leer num[i]		
	Fin Para
	//Fin de ingreso de datos.
	//determinaci�n de pares:
	sumaTotal=0
	sumaimapares=0
	promImpares=0
	nPares=0
	nImpares=0
	Para i=1 Hasta 10 Con Paso 1 Hacer
		Si (num[i]%2=0) Entonces
			nPares=nPares+1
		SiNo
			nImpares= nImpares+1
			sumaImpares= sumaImpares+ num[i]
		Fin Si
		sumaTotal=sumaTotal+num[i]
	Fin Para
	promImpares = sumaImpares/nImpares
	
	Escribir "La cantidad de n�meros pares es: ",nPares
	Escribir "la suma Total de los numeros ingresados es: ",sumaTotal
	Escribir "El promedio de los n�meros es Impares es:", promImpares
FinProceso
