'''
Dada una secuencia de números leídos por teclado, que acabe con un –1, por ejemplo: 5,3,0,2,4,4,0,0,2,3,6,0,……,-1; Realizar un método que calcule la media aritmética. Suponemos que el usuario no insertara numero negativos.
'''
suma: int = 0
media: str = ""

while True:
    try:
        flag = True
        i = 0
        while flag :
            num = int(input("Ingrese un numero Entero: "))
            if num != -1:
                suma += num
                i += 1
            else:
                flag = False

        # Analizando Division entre 0
        media = "ND" if i == 0 else str(suma/i)

        print(f"La media es: {media}")

        break
    except ValueError:
        print(f"Ingresar valores validos (Numeros)")
    except Exception as error:
        print(f"Se ha encontrado un error: {error}")