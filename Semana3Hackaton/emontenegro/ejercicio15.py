'''
Dados 10 números enteros que se ingresan por teclado, calcular cuántos de ellos son pares, cuánto suman ellos y el promedio de los impares
'''

cantidadPares: int = 0
sumaPares: int = 0
sumaImpares: int = 0
cantidadImpares: int = 0

while True:
    try:

        for i in range(1, 11):
            numero = int(input("Ingrese Numero: "))

            if numero % 2 == 0:
                cantidadPares += 1
                sumaPares += numero
            else:
                cantidadImpares += 1
                sumaImpares += numero

        print(f"Cantidad de Pares: {cantidadPares}")
        print(f"Suma de Pares: {sumaPares}")
        print(f"Promedio Impares: {(sumaImpares/cantidadImpares)}")

        break
    except ValueError:
        print(f"Ingresar valores validos (Numeros)")
    except Exception as error:
        print(f"Se ha encontrado un error: {error}")