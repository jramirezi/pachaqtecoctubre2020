'''
Escribir un método de un programa que permita leer la edad y peso de una persona y posteriormente imprimirla
'''

while True:
    try:
        edad = int(input("Digitar tu edad en Numeros: "))
        peso = float(input("Digitar tu peso en Numeros: "))
        print(f"Tu edad es {edad} y tu peso es {peso}")
        break
    except ValueError:
        print(f"Ingresar valores validos (Numeros)")
    except Exception as error:
        print(f"Se ha encontrado un error: {error}")