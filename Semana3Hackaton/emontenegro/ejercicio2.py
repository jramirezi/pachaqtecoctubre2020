'''
Escribir un método que calcule el área de un triángulo recibiendo como entrada el valor de base y altura
'''

while True:
    try:
        base = float(input("Digitar la base: "))
        altura = float(input("Digitar la altura: "))
        area = (base * altura) / 2
        print(f"El area del triaungulo es: {area}")
        break
    except ValueError:
        print(f"Ingresar valores validos (Numeros)")
    except Exception as error:
        print(f"Se ha encontrado un error: {error}")