'''
Un método que lea números enteros hasta teclear 0, y nos muestre el máximo, el mínimo y la media de todos ellos. Piensa como debemos inicializar las variables
'''
maximo: int = 0
minimo: int = 0
suma: int = 0
media: str = ""

while True:
    try:
        flag = True
        i = 0
        while flag :
            num = int(input("Ingrese un numero Entero: "))
            if num != 0:
                # Asignamos el Mayor
                if maximo == 0:
                    maximo = num
                else:
                    if num > maximo:
                        maximo = num;
                    
                # Asignamos el Menor
                if minimo == 0:
                    minimo = num
                else:
                    if num < minimo:
                        minimo = num;

                # Asignacion del Promedio
                suma += num # suma = suma + num
                i += 1
            else:
                flag = False

        # Analizando Division entre 0
        media = "ND" if i == 0 else str(suma/i)

        print(f"El numero maximo es: {maximo}")
        print(f"El numero minimo es: {minimo}")
        print(f"La media es: {media}")

        break
    except ValueError:
        print(f"Ingresar valores validos (Numeros)")
    except Exception as error:
        print(f"Se ha encontrado un error: {error}")