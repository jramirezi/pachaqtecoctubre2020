import psycopg2

conexion = psycopg2.connect(user= 'postgres', password= '12345', host= '127.0.0.1',port= '5432', database ='JJRAMIREZ')

cursor = conexion.cursor()
sentencia = 'SELECT * FROM alumno
#variable cursor ejecutando la consulta sql definido en la variable sentencia
cursor.execute(sentencia)
#metodo para que regrese la informacion del cursor de la sentencia sql
registros = cursor.fetchall()
print(registros)
#cerrar el cursor y conexion 
cursor.close()
conexion.close()