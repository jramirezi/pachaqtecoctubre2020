from utils import conexionBDD, Menu
from time import sleep
from modelos import Pinturas, Pinceles, Marca, Tipo


#Variables Globales

arrPinceles = []
arrPinturas = []
arrMarca = []
arrTipo = []
# conn = Conexion("mongodb://localhost:27017", "Pinturas")

# conn = pymongo.MongoClient("mongodb+srv://alumnos:7X6PHavJCtiSmQex@cluster0.b3wca.mongodb.net/<dbname>?retryWrites=true&w=majority")
# db = client.test

conn = conexionBDD(3)

conn2 = conexionBDD(2)

conn2.
def cargaInicial():
    #Cargar Pinturas
    result = conn.obtener_registros("pinturas", {})
    for obj in result:
        objpintura = Pinturas(
            obj['color'], obj['codigo'], obj['marca'], obj['referencia'], obj['tipo'])
        arrPinturas.append(objpintura)

    result = conn.obtener_registros("pinceles", {})
    for obj in result:
        objpinceles = Pinceles(obj['codigo'], obj['marca'])
        arrPinceles.append(objpinceles)

    result = conn.obtener_registros("tipo", {})
    for obj in result:
        objtipo = Tipo(obj['nombre'])
        arrTipo.append(objtipo)
    
    result = conn.obtener_registros("marca", {})
    for obj in result:
        objmarca = Marca(obj['nombre'])
        arrMarca.append(objmarca)

def buscarPinturas(color):
    lstEncontrado = []
    for objPinturas in arrPinturas:
        if(objPinturas.color.find(color) == -1):
            print("No encontros")
        else:
            print(objPinturas)
            lstEncontrado.append(objPinturas)
    return lstEncontrado
cargaInicial()

opMenuPrincipal = {"\t- Agregar": 1, "\t- Buscar": 2,
                   "\t- Actualizar": 3, "\t- Eliminar": 4}

menuPrincipal = Menu("Menu Principal", opMenuPrincipal)
salirMenu = True


while salirMenu:
    result = menuPrincipal.mostrarMenu()
    if(result == 1):
        color = input("Ingrese el color: ")
        codigo = input("Ingrese el codigo: ")
        marca = input("Ingrese la marca: ")
        referencia = input("Ingrese la referencia: ")
        tipo = input("Ingrese el Tipo de Pintura: ")    
        objpintura = Pinturas(color,codigo,marca,referencia,tipo)
        if(conn.insertar_registro("pinturas", objpintura.toDict())):
            arrPinturas.append(objpintura)
            print("Se ha insertado correctamente")
            sleep(3)
        else:
            print("Hubo un error")
    if(result == 2):
        color = input("Ingrese el color a buscar: ")
        result = buscarPinturas(color)
        
        if(len(result)==0):
            print("no se ha encontrado el color")
        else:
            for obj in result:
                print(f"Color: {obj.color}, Codigo: {obj.codigo}, Referencia: {obj.referencia}")
            sleep(5)
        query = {"color": {"$regex": f'{color}', "$options": 'i'}}
        print(query)
        dicCondicion = {"color": f'{color}'}
        result = conn.obtener_registros("pinturas", query)
        # if not result:
        #     print("hubo un error")
        #     sleep(3)
        # else:
        #print(result)
        for obj in result:
            print(
                f"Color: {obj['color']}, Codigo: {obj['codigo']}, Referencia: {obj['referencia']}")
        sleep(5)
    if(result == 3):
        color = input("Ingrese el color a actualizar: ")
        newColor = input("Ingrese el nuevo color: ")
        conn.actualizar_registro(
            "pinturas", {"color": color}, {"color": newColor})
        for objPinturas in arrPinturas:
            if(objPinturas.color.find(color) == -1):
                print("No encontros")
            else:
                objPinturas.color = newColor    

    if(result == 4):
        color = input("Ingrese el color a borrar: ")
        conn.eliminar_registro("pinturas", {"color": color})
        for objPinturas in arrPinturas:
            if(objPinturas.color.find(color) == -1):
                print("No encontros")
            else:
                arrPinturas.remove(objPinturas)
    if(result == 9):
        print("salir")
        salirMenu = False
