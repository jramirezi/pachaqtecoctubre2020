# BACK END
## Un desarrollador Front-end
Aquella persona que se encarga de desarrollar lo que esta de cara al cliente (toda la parte visual)
lo que el cliente final ve al ingresar a tu pagina.

### Tecnologías de desarrollo que un Front-End debe conocer
Aparte del clasico HTML(Estrutura de la pagina), CSS(Apariencia que tendra la pagina a traves de estilos), JS (Nos brindda la interactividad),
Debemos entender que un navegador solo reconoce esas 3 tecnologias, pero en el camino aparecen herramientas que facilitan la labor del frontend
como son JADE(Motor de plantillas), SASS(Preprocesador de CSS), Typescript(Lenguaje de Programacion que prentende facilitar la escritura de JS)

### Actividades de un Front-end
Las actividades normalmente dependen un poco de la empresa y las responsabilidades que se te asignan
pero comunmente el frontend se puede encargar de: 
- Desarrollo del Prototipo de la web usando herramientas como sketch, adobe XD, figma
- Crear la estructura base del Proyecto apoyandose en ciertos frameworks para ahorrarse trabajo o tambien hacerlo de forma manual
- Implementar la pagina basandose en el prototipo.
- Implementar las pruebas unitarias
- Subir los cambios a la rama de desarrollo
- Comunicarse con la gente de QA, DBA, Infra para el resto de los pasos.

### Empresas que aportan al desarrollo Front-end
Google con el apoyo al Framework Angular
Facebook con el apoyo a la libreria React
Laravel con el apoyo a la libreria VueJS

### ¿Cuál es el rol de un Front-end en las empresas del Perú?
El rol de un frontend aqui en peru o en el mundo es de implementar una app
que sea visualmente atractiva (UI), y que brinde una experiencia agradable al usuario (UX),
que la aplicacion se centre el proposito para el cual fue pensado y que cumpla con estandares de calidad.

### ¿Cuál es el rol de un Front-end en las empresas del mundo?
El rol de un frontend aqui en peru o en el mundo es de implementar una app
que sea visualmente atractiva (UI), y que brinde una experiencia agradable al usuario (UX),
que la aplicacion se centre el proposito para el cual fue pensado y que cumpla con estandares de calidad.

### Analicemos: ¿Qué es lo que más destaca en las siguientes páginas web?
Wordpress y GitLab

### Conclusiones
Wordpress: 
Es un CMS que la cual se nota que es ligero, facil de usar y cuenta con buena interfaz sin tanto contenido que confunda y a la vez es potente

GitLab:
Notamos que esta desarrollado usando una libreria ligera Vue, su interfaz es muy Limpia y cuenta con un buen esquema de colores.

### Tendencias visuales en el diseño web 2020
Fuentes: Tamaño adecuado de las fuentes y diferentes tipos para que la pagina tenga una apariencia diferente y que a su ves sea legible en diferentes dispositivos.
Parallax: Efecto en las paginas de tipo Landing Page para mostrar tu producto una forma muy llamativa.

### Roadmap de un desarrollador Front-end
### Tendencias para Front-end 2020:
PWA: Progresive Web Application nos permite usar una pagina web como si fuera una aplicacion nativa haciendo uso de los services Workers
Web Assembly: Nos permite potenciar nuestra aplicacion Frontend dandole el poder de ejecutar tu codigo de forma casi nativa.
Server Less: Apoya a los programadore Front al evitar que sean dependientes de un backend en si, haciendo uso de funciones en la nube para realizar las labores backend
y evitando pensar en el escalamiento ya se que escala de forma automatica.

## Un desarrollador Back-end

### Tecnologías de desarrollo que un Back-End debe conocer
- Manejo almenos basico de la linea de comandos.
- Uso un sistema de control de versiones (Git).
- Conocer almenos lenguaje de Programacion como puede ser Java, C#, Python.
- Manejar ciertos frameworks para facilitar el desarrollo
- Manejo de Leguaje de Base de Datos.
- Uso de contenedores (Docker)

### Lenguajes de programación y Frameworks 
Lenguajes:
Java, C#, Python, Ruby, PHP
Frameworks:
Spring, Django, Ruby on Rails, Symfony, Laravel

### Bases de datos
Relacionales:
Oracle
SQL Server
Postgress
Mysql
Maria DB

No Relacionales:
Mongo DB
Dynamo DB
Cassandra

### Servidores web y protocolos
Nginx 
Apache
Apache Tomcat
IIS

### Actividades de un Back-end
Analizar, Coordinar, Desarrollar.

### Conceptos que debe conocer un desarrollador Back-end
Performance, Infraestructura, Automatizacion, Seguridad, un poco de Redes.

### Roadmap de un desarrollador Back-end
### Tendencias para Back-end 2020:
Lenguajes:
Python -> Se esta volviendo un lenguaje muy popular por su facilidad de uso la gran catidad de herramientas desarolladas en ese lenguaje
el uso en big data, inteligencia artificial, etc.

### Analicemos: ¿Dónde crees que pueda estar el backend en las siguientes paginas? 
pienso a que el backend se va a volver mas como un servicio independiente para realizar labores especificas,
lo que actualmente se esta empezando a hacer con Server Less.

### Conclusiones
Cada Año aparecen nuevas tecnologias, lenguajes que van siendo cada ves mas utiles, mejoras en rendimiento,
y herramientas que facilitan la labor del desarrollo y su crecimiento.