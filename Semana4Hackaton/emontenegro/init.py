from models import Libro, Autor, Editorial, Archivo, Alumno, Bibliotecario

# Guardamos los Autores
objArchivo = Archivo("autores.txt")
objAutor1 = Autor(1, "34568734", "Javier", "Santaolalla", "01/01/1985", "M")
objAutor2 = Autor(2, "34549712", "Bruno", "Montenegro", "01/01/1994", "M")
objArchivo.AgregarElemento(objAutor1)
objArchivo.AgregarElemento(objAutor2)

# Guardamos las Editoriales
objArchivo = Archivo("editoriales.txt")
objEditorial = Editorial(1, "0009999999", "La Esfera De Los Libros")
objArchivo.AgregarElemento(objEditorial)

# Guardamos los Libros
objArchivo = Archivo("libros.txt")
objArchivo.AgregarElemento(Libro(1, "Agujeros Negros", objAutor1, "Primera","Ciencias Exactas", "9788490607725", objEditorial, 5))
objArchivo.AgregarElemento(Libro(2, "La Odisea", objAutor1, "Segunda","Obras Literarias", "9788490607787", objEditorial, 8))
objArchivo.AgregarElemento(Libro(3, "La Iliada", objAutor2, "Primera","Obras Literarias", "9788490607721", objEditorial, 13))
objArchivo.AgregarElemento(Libro(4, "La Divina Comedia", objAutor2, "Segunda","Obras Literarias", "9788490607713", objEditorial, 2))
objArchivo.AgregarElemento(Libro(5, "Mi Planta de Naranja Lima", objAutor2, "Tercera","Obras Literarias", "9788490607700", objEditorial, 26))

# Guardamos los Alumnos
objArchivo = Archivo("alumnos.txt")
objArchivo.AgregarElemento(Alumno(1, "457869856", "Roberto", "Medina", "01/04/89", "M"))
objArchivo.AgregarElemento(Alumno(2, "457869844", "Mario", "Cabanillas", "13/08/92", "M"))
objArchivo.AgregarElemento(Alumno(4, "457869888", "Maria", "Fernandez", "06/05/85", "F"))

# Guardamos los Bibliotecarios
objArchivo = Archivo("bibliotecarios.txt")
objArchivo.AgregarElemento(Bibliotecario(1, "89764278", "Maria", "Magdalena", "07/12/89", "F"))
objArchivo.AgregarElemento(Bibliotecario(2, "89334223", "Jose", "Carrillo", "04/10/89", "M"))