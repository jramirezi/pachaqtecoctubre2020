import os
class Persona:
    def __init__(self, DNI, Nombre, Apellido, FechaNacimiento, Sexo):
        self.DNI = DNI
        self.Nombre = Nombre
        self.Apellido = Apellido
        self.FechaNacimiento = FechaNacimiento
        self.Sexo = Sexo


class Bibliotecario(Persona):
    def __init__(self, DNI, Nombre, Apellido, FechaNacimiento, Sexo, CodigoEmpleado):
        super().__init__(DNI, Nombre, Apellido, FechaNacimiento, Sexo)
        self.CodigoEmpleado = CodigoEmpleado

    def PrestarLibro(self, Libro):
        pass

    def BuscarLibro(self, Libro):
        pass

    def RecibirLibro(self, Libro):
        pass


class Alumno(Persona):
    def __init__(self, DNI, Nombre, Apellido, FechaNacimiento, Sexo, CodigoAlumno):
        super().__init__(DNI, Nombre, Apellido, FechaNacimiento, Sexo)
        self.CodigoAlumno = CodigoAlumno

    def PedirPrestado(self, Libro):
        Libro.Cantidad = Libro.Cantidad - 1

    def DevolverLibro(self, Libro):
        Libro.Cantidad = Libro.Cantidad + 1


class Libro:
    def __init__(self, CodigoLibro, Titulo, Autor, Edicion, Categoria, ISBN, Editorial, Cantidad):
        self.CodigoLibro = CodigoLibro
        self.Titulo = Titulo
        self.Autor = Autor
        self.Edicion = Edicion
        self.Categoria = Categoria
        self.ISBN = ISBN
        self.Editorial = Editorial
        self.Cantidad = Cantidad


class Autor(Persona):
    def __init__(self, DNI, Nombre, Apellido, FechaNacimiento, Sexo, CodigoAutor):
        super().__init__(DNI, Nombre, Apellido, FechaNacimiento, Sexo)
        self.CodigoAutor = CodigoAutor


class Editorial:
    def __init__(self, CodigoEditorial, RUC, Nombre):
        self.CodigoEditorial = CodigoEditorial
        self.RUC = RUC
        self.Nombre = Nombre

class Archivo:
    def __init__(self, NombreArchivo):
        self.NombreArchivo = NombreArchivo
    
    def MostrarArchivo(self):
        try:
            file = open(self.NombreArchivo, 'r')
            for linea in file.readlines():
                print(linea)
        except Exception as error:
            print(f"Ha ocurrido un error: {error}")
        else:
            file.close()
    def AgregarElemento(self, Libro):
        try:
            file = open(self.NombreArchivo,"a")
            texto = f"{Libro.Titulo}, {Libro.Autor.Nombre},{Libro.Edicion}\n"
            file.write(texto)
        except FileNotFoundError as Error:
            file = open(self.NombreArchivo, "w")
        except Exception as Excp:
            print(f"Ocurrio un Error: {Excp}")
        else:
            file.close()