'''
Esta es una calculadora sencilla
'''
import funcionesAritmeticas

print("== Bienvenido a mi Calculadora ==")
print("\tMenu ========")
print("\t[1] Suma")
print("\t[2] Resta")
print("\t[3] Multiplicacion")
print("\t[4] Division")
print("\t[5] Potenciacion")
print("\t[0] Salir")

menu=("Salir", "Suma", "Resta", "Multiplicacion", "Division", "Potenciacion")

operacion = int(input("Elige una operacion: "))

if(operacion > len(menu) or operacion < 0):
    print("Operacion no valida")

else: 
    print("Elegiste la operacion " + menu[operacion])

    numero1 = int(input("Ingresa el primer numero: "))
    numero2 = int(input("Ingresa el segundo numero: "))

    if(operacion == 0):
        print("Gracias por usar la calculadora!")

    if(operacion == 1):
        print("La Suma de ",numero1, " y ", numero2," es ",funcionesAritmeticas.Suma(numero1,numero2))

    if(operacion == 2):
        print("La Resta de ",numero1, " y ", numero2," es ",funcionesAritmeticas.Resta(numero1,numero2))

    if(operacion == 3):
        print("La Multiplicacion de ",numero1, " y ", numero2," es ",funcionesAritmeticas.Multiplicacion(numero1,numero2))

    if(operacion == 4):
        print("La Division de ",numero1, " y ", numero2," es ",funcionesAritmeticas.Division(numero1,numero2))

    if(operacion == 5):
        print("La Potencia de ",numero1, " y ", numero2," es ",funcionesAritmeticas.Potencia(numero1,numero2))
